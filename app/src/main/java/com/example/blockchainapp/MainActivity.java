package com.example.blockchainapp;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {

    private ArrayList<BlockItem> mBlockList;

    private RecyclerView mRecyclerView;
    private RecyclerView.Adapter mAdapter;
    private RecyclerView.LayoutManager mLayoutManager;

    private ImageButton buttonInsert;
    private EditText editTextInsert;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        createBlockList();
        buildRecyclerView();

        buttonInsert = findViewById(R.id.button_insert);
        editTextInsert = findViewById(R.id.edittext_insert);

        buttonInsert.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                int position = mBlockList.size();
                String data = editTextInsert.getText().toString();
                editTextInsert.setText("");
                insertItem(position, data);
                hideKeybaord(v);

            }
        });


    }

    public void insertItem(int position, String data){
        mBlockList.add(position, new BlockItem(mBlockList.size(),System.currentTimeMillis(),mBlockList.get(mBlockList.size()-1).getHash(),BlockItem.encryptIt(data)));
        mAdapter.notifyItemInserted(position);

    }

    public void createBlockList() {
        mBlockList = new ArrayList<>();
        mBlockList.add(new BlockItem(0, System.currentTimeMillis(),null,"Genesis Block"));

    }

    public void buildRecyclerView() {
        mRecyclerView = findViewById(R.id.recyclerView);
        mRecyclerView.setHasFixedSize(true);
        mLayoutManager = new LinearLayoutManager(this);
        mAdapter = new BlockAdapter(mBlockList);

        mRecyclerView.setLayoutManager(mLayoutManager);
        mRecyclerView.setAdapter(mAdapter);

    }

    private void hideKeybaord(View v) {
        InputMethodManager inputMethodManager = (InputMethodManager)getSystemService(INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(v.getApplicationWindowToken(),0);
    }
}