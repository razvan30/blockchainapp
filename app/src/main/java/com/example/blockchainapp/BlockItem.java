package com.example.blockchainapp;

import android.util.Base64;

import androidx.annotation.NonNull;

import java.nio.charset.StandardCharsets;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.spec.InvalidKeySpecException;

import javax.crypto.BadPaddingException;
import javax.crypto.Cipher;
import javax.crypto.IllegalBlockSizeException;
import javax.crypto.NoSuchPaddingException;
import javax.crypto.SecretKey;
import javax.crypto.SecretKeyFactory;
import javax.crypto.spec.DESKeySpec;

public class BlockItem {


    private String hash,previousHash,data;
    private int index;
    private long timestamp;
    private static final String PASSWORD = "CRYPTOGRAPHY IS THE BEST";
    private static final String ALGORITHM = "DES";


    public BlockItem(int index, long timestamp, String previousHash, String data) {

        this.index = index;
        this.timestamp = timestamp;
        this.previousHash = previousHash;
        this.data = data;

        hash = BlockItem.calculateHash_detail(this);
    }


    public static String calculateHash_detail(BlockItem blockItem) {

        if(blockItem != null)
        {
            MessageDigest messageDigest;
            try {
                messageDigest=MessageDigest.getInstance("SHA-256");

            }
            catch (NoSuchAlgorithmException e){
                return null;
            }

            String txt = blockItem.str();
            final byte[] bytes  = messageDigest.digest(txt.getBytes());
            final StringBuilder builder = new StringBuilder();
            for (final byte b: bytes){
                String hex = Integer.toHexString(0xff & b);
                if(hex.length() == 1){
                    builder.append('0');
                }
                builder.append(hex);
            }
            return builder.toString();
        }
        return null;
    }

    private String str() {
        return index + timestamp + previousHash + data;
    }

    public int getIndex() {
        return index;
    }

    public long getTimestamp() {
        return timestamp;
    }

    public String getHash() {
        return hash;
    }

    public String getPreviousHash() {
        return previousHash;
    }
    public String getData() {
        return data;
    }


    public static String encryptIt(@NonNull String value) {

        try {
            DESKeySpec keySpec = new DESKeySpec(PASSWORD.getBytes(StandardCharsets.UTF_8));
            SecretKeyFactory keyFactory = SecretKeyFactory.getInstance(ALGORITHM);
            SecretKey key = keyFactory.generateSecret(keySpec);
            byte[] clearText = value.getBytes(StandardCharsets.UTF_8);
            Cipher cipher = Cipher.getInstance(ALGORITHM);
            cipher.init(Cipher.ENCRYPT_MODE,key);
            return Base64.encodeToString(cipher.doFinal(clearText),Base64.DEFAULT);
        }

        catch (InvalidKeyException | InvalidKeySpecException | NoSuchAlgorithmException | BadPaddingException | NoSuchPaddingException
                | IllegalBlockSizeException e)
        {
            e.printStackTrace();
        }
        return value;
    }




}
