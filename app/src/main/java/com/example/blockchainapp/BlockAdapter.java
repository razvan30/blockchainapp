package com.example.blockchainapp;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import org.w3c.dom.Text;

import java.util.ArrayList;
import java.util.Date;

public class BlockAdapter extends RecyclerView.Adapter<BlockAdapter.BlockViewHolder> {

    private ArrayList<BlockItem> mBlockList;

    public static class BlockViewHolder extends RecyclerView.ViewHolder{
        public TextView mTextView1,mTextView2,mTextView3,mTextView4,mTextView5;
        public TextView previoushash,timestamp,data,currenthash,blocknumber;

        public BlockViewHolder(@NonNull View itemView) {
            super(itemView);
            mTextView1 = itemView.findViewById(R.id.textView);
            mTextView2 = itemView.findViewById(R.id.textView2);
            mTextView3 = itemView.findViewById(R.id.textView3);
            mTextView4 = itemView.findViewById(R.id.textView4);
            mTextView5 = itemView.findViewById(R.id.textView5);
            previoushash = itemView.findViewById(R.id.txt_previous_hash);
            timestamp = itemView.findViewById(R.id.txt_timestamp);
            blocknumber = itemView.findViewById(R.id.blocknumber);
            data = itemView.findViewById(R.id.txt_data);
            currenthash = itemView.findViewById(R.id.txt_hash);

        }
    }

    public BlockAdapter(ArrayList<BlockItem> blockList) {
        mBlockList = blockList;
    }

    @NonNull
    @Override
    public BlockViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.block_item,parent,false);
        BlockViewHolder bvh = new BlockViewHolder(v);
        return bvh;
    }

    @Override
    public void onBindViewHolder(@NonNull BlockViewHolder holder, int position) {
        BlockItem currentItem =  mBlockList.get(position);

        holder.blocknumber.setText(String.valueOf(currentItem.getIndex()));
        holder.previoushash.setText(currentItem.getPreviousHash());
        holder.timestamp.setText(String.valueOf(new Date(currentItem.getTimestamp())));
        holder.data.setText(currentItem.getData());
        holder.currenthash.setText(currentItem.getHash());

    }

    @Override
    public int getItemCount() {
        return mBlockList.size();
    }
}
